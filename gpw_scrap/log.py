import logging


def setup_logging():
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s,%(msecs)d %(levelname)-6s [%(filename)s:%(lineno)d] %(message)s",
    )
