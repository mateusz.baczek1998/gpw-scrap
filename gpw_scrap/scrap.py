import logging
import random
from .log import setup_logging
import time
import traceback
from pathlib import Path
import dataset
from datetime import datetime
import sqlite3

import requests
from prometheus_client import Counter, Summary, start_http_server, Gauge
from bs4 import BeautifulSoup

logger = logging.getLogger(__name__)

COMPANY_SCRAPING_TIME = Summary(
    "gpw_company_scraping_time_seconds", "Time spent scraping companies"
)
SCRAPING_TIME = Summary("gpw_scraping_time_seconds", "Time spent scraping instruments")
SCRAPING_ERRORS = Gauge("gpw_scrape_failures", "Number of failures during scrape")
LAST_SCRAPE_COMPANIES_TOTAL = Gauge(
    "gpw_last_scrape_num_companies", "Number of companies from last scrape"
)
SCRAP_PROGRESS = Gauge("gpw_scrap_progress", "Scrape progress")
NUM_DATA_POINTS = Gauge(
    "gpw_data_points_total", "Number of data points per company", ["isin"]
)
NUM_COMPANIES_IN_DB_TOTAL = Gauge(
    "gpw_companies_in_db_total", "Total number of companies in database"
)
DATA_DIR_SIZE_BYTES = Gauge("gpw_data_dir_size_bytes", "Size of the data directory")

DATA_DIR = Path("data")
DATABASE_PATH = str((DATA_DIR / "database.db").absolute())
db = dataset.connect("sqlite:///" + DATABASE_PATH)


def get_url_for(instrument: str) -> str:
    return (
        "https://www.gpw.pl/chart-json.php?req=[{%22isin%22:%22"
        + instrument
        + "%22,%22mode%22:%22CURR%22,%22from%22:464648,%22to%22:null}]&t=1672743876247"
    )


@SCRAPING_TIME.time()
def get_scrape_response(instrument: str):
    logger.info("Scraping data for %s", instrument)
    url = get_url_for(instrument)
    response = requests.get(url)
    json = response.json()
    data = json[0]["data"]

    return data


def save_data_to_file(instrument: str, data: dict):
    table = db["stock_values"]
    # https://dataset.readthedocs.io/en/latest/api.html#dataset.Table.insert_ignore
    for point in data:
        table.insert_ignore(
            {
                "isin": instrument,
                "timestamp": datetime.fromtimestamp(point["t"]),
                "volume": point["v"],
                "o": point["o"],
                "c": point["c"],
                "l": point["l"],
                "h": point["h"],
                "p": point["p"],
            },
            ["isin", "timestamp"],
        )

    # Fuck prepared statements, this is a side project
    num_records_by_isin = db.query(
        f"SELECT COUNT(*) AS count FROM stock_values WHERE isin='{instrument}'"
    )
    for row in num_records_by_isin:
        NUM_DATA_POINTS.labels(isin=instrument).set(row["count"])

    total_num_of_companies = db.query(
        f"SELECT COUNT(distinct(isin)) AS count FROM stock_values"
    )
    for row in total_num_of_companies:
        NUM_COMPANIES_IN_DB_TOTAL.set(row["count"])


def perform_scrape(instrument: str):
    data = get_scrape_response(instrument)
    save_data_to_file(instrument, data)


@COMPANY_SCRAPING_TIME.time()
def scrape_companies() -> list[str]:
    """Return current list of all company ISIN numbers on the market"""

    logger.info("Fetching companies data")
    companies_response = requests.get(
        "https://www.gpw.pl/ajaxindex.php?action=GPWQuotations&start=showTable&tab=all&lang=PL&type=&full=1&format=html&download_xls=1"
    )
    companies_response.raise_for_status()

    logger.info("Parsing companies response")
    soup = BeautifulSoup(companies_response.text, "html.parser")
    isin_table_tds = soup.find_all("td", {"class": "left col3"})

    isins = []
    for isin_td in isin_table_tds:
        isin = isin_td.text
        if not isin.startswith("PL"):
            # TODO: is this really needed?
            logger.info("Discarding isin that does not start with 'PL': %s", isin)
            continue

        if len(isin) != 12:
            logger.warning("Discarding isin with invalid length: %s", isin)
            SCRAPING_ERRORS.inc()
            continue
        isins.append(isin)

    LAST_SCRAPE_COMPANIES_TOTAL.set(len(isins))

    return isins


def scrape_all(instruments: list[str]):
    SCRAPING_ERRORS.set(0)
    for instrument_num, instrument in enumerate(instruments):
        SCRAP_PROGRESS.set(instrument_num)
        DATA_DIR_SIZE_BYTES.set(sum(p.stat().st_size for p in DATA_DIR.rglob("*")))
        try:
            perform_scrape(instrument)
            logger.info("Taking a break between scraps..")
            time.sleep(random.uniform(10.0, 20.0))
        except:
            SCRAPING_ERRORS.inc()
            logger.exception(traceback.format_exc())


if __name__ == "__main__":
    setup_logging()
    start_http_server(8092)

    logger.info("Setting up sqlite connection")
    connection = sqlite3.connect(DATABASE_PATH)

    logger.info("Ensuring data directory exists")
    DATA_DIR.mkdir(exist_ok=True)

    sleep_time = 3600 * 3

    while True:
        instruments = scrape_companies()
        scrape_all(instruments)
        logger.info("Sleeping for %i seconds", sleep_time)
        time.sleep(sleep_time)
