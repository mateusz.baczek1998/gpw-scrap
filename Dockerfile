FROM archlinux:latest

RUN pacman -Sy --noconfirm poetry && pacman -Sc --noconfirm

WORKDIR /app

COPY poetry.lock .
COPY pyproject.toml .
RUN poetry install --only main --no-root

COPY gpw_scrap /app/gpw_scrap
CMD ["poetry", "run", "python", "-m", "gpw_scrap.scrap"]
